Ext.define('Pertemuan9.view.main.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',

    requires: [
        'Pertemuan9.store.StoreBar',
        'Ext.grid.plugin.Editable'
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'List Bar',

    // store: {
    //     type: 'personnel'
    // },

    bind: '{storebar}',

    viewModel: {
        stores: {
            storebar: {
                type: 'storebar'
            }
        }
    },

    columns: [
        { text: 'Name', dataIndex: 'name', width: 150, editable: true},
        { text: 'g1', dataIndex: 'g1', width: 150, editable: true},
        { text: 'g2', dataIndex: 'g2', width: 150, editable: true},
        { text: 'g3', dataIndex: 'g3', width: 150, editable: true},
        { text: 'g4', dataIndex: 'g4', width: 150, editable: true},
        { text: 'g5', dataIndex: 'g5', width: 150, editable: true},
        { text: 'g6', dataIndex: 'g6', width: 150, editable: true}
    ],

    listeners: {
        select: 'onBarItemSelected'
    }
});
